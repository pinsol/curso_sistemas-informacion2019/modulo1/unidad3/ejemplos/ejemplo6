﻿                                              /** Modulo I - Unidad 3 - Hoja Ejemplos 6 **/
USE ejemploprogramacion6;

/*
  Ejemplo 1-
  Disparador para la tabla ventas para que cuando metas un registro nuevo te
  calcule el total automáticamente.
*/
DELIMITER //
CREATE OR REPLACE TRIGGER ventasBI
BEFORE INSERT
  ON ventas
  FOR EACH ROW
BEGIN
  set new.total = new.unidades*new.precio;
END //
DELIMITER ;

SELECT * FROM ventas v;

-- insertamos nuevo dato
INSERT INTO ventas (producto, precio, unidades)
  VALUES ('p5', 30, 10);

-- comprobacion consultando la tabla
SELECT * FROM ventas v;


/*
  Ejemplo 2-
  Disparador para la tabla ventas para que cuando inserte un registro me sume
  el total a la tabla productos (en el campo cantidad).
*/
DELIMITER //
CREATE OR REPLACE TRIGGER ventasAI
AFTER INSERT
  ON ventas
  FOR EACH ROW
BEGIN
  UPDATE productos p
    SET cantidad = p.cantidad + NEW.total WHERE p.producto=NEW.producto;
END //
DELIMITER ;

INSERT INTO ventas (producto, precio, unidades)
  VALUES ('p4', 30, 50);

SELECT * FROM ventas v;
SELECT * FROM productos p;

/*
  Ejemplo 3-
  Disparador para la tabla ventas para que cuando actualices un registro nuevo
  te calcule el total automáticamente.
*/
DELIMITER //
CREATE OR REPLACE TRIGGER ventasBU
BEFORE UPDATE
  ON ventas
  FOR EACH ROW
BEGIN
  set new.total = new.unidades*new.precio;
END //
DELIMITER ;

UPDATE ventas v
  SET v.unidades = 10
WHERE v.id = 6;

SELECT * FROM ventas v;

/*
  Ejemplo 4-
  Disparador para la tabla ventas para que cuando actualice un registro me
  sume el total a la tabla productos (en el campo cantidad).
*/
DELIMITER //
CREATE OR REPLACE TRIGGER ventasAU
AFTER UPDATE
  ON ventas
  FOR EACH ROW
BEGIN
  UPDATE productos p
    SET cantidad = p.cantidad + (NEW.total - old.total) WHERE p.producto=NEW.producto;
END //
DELIMITER ;

UPDATE ventas v
  SET v.unidades = 3
WHERE v.id = 3;

SELECT * FROM ventas v;

/*
  Ejemplo 5-
  Disparador para la tabla productos que si cambia el código del producto te
  sume todos los totales de ese producto de la tabla ventas
*/
DELIMITER //
CREATE OR REPLACE TRIGGER productosBU
BEFORE UPDATE
  ON productos
  FOR EACH ROW
BEGIN
  set new.cantidad =
                     (
                      SELECT
                        SUM(v.total) 
                      FROM
                        ventas v 
                      WHERE
                        v.producto=new.producto
                     );
END //
DELIMITER ;

SELECT * FROM productos p;

UPDATE productos p
  set p.producto = 'p6' WHERE p.producto = 'p3';

SELECT * FROM productos p;

/*
  Ejemplo 6-
  Disparador para la tabla productos que si eliminas
  un producto te elimine todos los productos del mismo código
  en la tabla ventas
*/
DELIMITER //
CREATE OR REPLACE TRIGGER productosAD
AFTER DELETE
  ON productos
  FOR EACH ROW
BEGIN
  DELETE FROM
    ventas
  WHERE
    producto = OLD.producto;
END //
DELIMITER ;

/*
  Ejemplo 7-
  Disparador para la tabla ventas que si eliminas
  un registro te reste el total del campo cantidad
  de la tabla productos (en el campo cantidad).
*/
DELIMITER //
CREATE OR REPLACE TRIGGER ventasAD
AFTER INSERT
  ON ventas
  FOR EACH ROW
BEGIN
  UPDATE productos p
    set p.cantidad = p.cantidad - old.total
    WHERE p.producto = old.producto;
END //
DELIMITER ;

/*
  Ejemplo 8-
  Modificar el disparador 3 para que modifique la tabla productos 
  actualizando el valor del campo cantidad en funcion del total.
*/
DELIMITER //
CREATE OR REPLACE TRIGGER ventasBU
BEFORE UPDATE
  ON ventas
  FOR EACH ROW
BEGIN
  set new.total = new.unidades*new.precio;

  UPDATE productos p 
        set p.cantidad = p.cantidad+(NEW.total-OLD.total) 
        WHERE p.producto = NEW.producto;
END //
DELIMITER ;

